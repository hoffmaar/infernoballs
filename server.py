#!/usr/bin/python
import json
import pendulum

import argparse
import regex
import threading
from json import JSONDecodeError
from unipath import Path

import hashes
from client_status import ClientStatus
from dataclasses import dataclass, astuple
from jsonrpclib.SimpleJSONRPCServer import SimpleJSONRPCServer
from typing import *

import enlighten

from work_store import *
import inferno

HASH_TYPE_CHECKER_PRIORITY: List[Tuple[str, Callable[[str], bool], int]] = [
    ('pbkdf', hashes.PBKDF.is_pbkdf, 4),
    ('sha1', hashes.is_sha1, 3),
    ('sha512', hashes.is_sha512, 2),
    ('argon2', hashes.is_argon2, 1)
]

CLIENT_DEAD_CHECK_INTERVAL = pendulum.duration(seconds=1)
CLIENT_DEAD_THRESHOLD = pendulum.duration(seconds=30)

BALL_CRACKED_CHECK_INTERVAL = pendulum.duration(seconds=1)

PRINT_INTERVAL = pendulum.duration(seconds=1)


@dataclass
class ClientProgress:
    speed: List[float]
    utilisation: List[float]
    guesses: Tuple[int, int]
    recovered: Tuple[int, int]
    time_started: pendulum.DateTime
    eta: pendulum.DateTime

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> 'ClientProgress':
        return cls(
            speed=[float(s) for s in d['speed']],
            utilisation=[float(u) for u in d['utilisation']],
            guesses=tuple(int(g) for g in d['guesses'])[:2],
            recovered=tuple(int(r) for r in d['recovered'])[:2],
            time_started=pendulum.from_timestamp(d['time_started']),
            eta=pendulum.from_timestamp(d['eta'])
        )


@dataclass
class Client:
    name: str
    last_heartbeat: pendulum.DateTime = pendulum.now()
    status: ClientStatus = ClientStatus.IDLE
    job: Optional[WorkBatch] = None
    progress: Optional[ClientProgress] = None


@dataclass
class _PasswordListItem:
    when: pendulum.DateTime
    hash: str
    password: str


@dataclass
class _OutputLine:
    who: str
    what: str


class RepeatingTimer(threading.Thread):
    def __init__(self,
                 interval: Union[int, float, pendulum.Duration],
                 target: Callable,
                 *arguments, **kwargs):
        super(RepeatingTimer, self).__init__(name=target.__name__)

        if isinstance(interval, pendulum.Duration):
            self.interval = interval.total_seconds()
        else:
            self.interval = float(interval)
        self.stopped = threading.Event()
        self.stopped.set()

        self.target = target
        self.arguments = arguments
        self.kwargs = kwargs

    def run(self):
        self.stopped.clear()
        while not self.stopped.wait(timeout=self.interval):
            self.target(*self.arguments, **self.kwargs)

    def cancel(self):
        self.stopped.set()


class Server:
    def __init__(self,
                 address: str,
                 port: int,
                 config_path: Path):
        self._rpc = SimpleJSONRPCServer((address, port))
        self._rpc.register_function(self._receive_heartbeat, 'heartbeat')
        self._rpc.register_function(self._receive_work_request, 'request_work')
        self._rpc.register_function(self._receive_new_password, 'new_password')
        self._rpc.register_function(self._receive_output_line, 'output_line')
        self._rpc.register_function(self._receive_error_line, 'error_line')
        self._rpc.register_function(self._receive_bye, 'bye')

        self._lock = threading.Lock()
        self._serve_thread = threading.Thread(target=self._rpc.serve_forever)
        self._dead_client_thread = RepeatingTimer(CLIENT_DEAD_CHECK_INTERVAL,
                                                  self._check_dead_clients)
        self._ball_cracked_thread = RepeatingTimer(BALL_CRACKED_CHECK_INTERVAL,
                                                   self._check_ball_cracked)
        self._print_thread = RepeatingTimer(PRINT_INTERVAL, self._print_output)

        self._clients: Dict[str, Client] = []  # key: Client.name
        self._output_lines: List[_OutputLine] = []
        self._error_lines: List[_OutputLine] = []
        self._passwords_discovered: List[_PasswordListItem] = []
        self._output_manager = enlighten.get_manager()

        # Initialised by refresh_config()
        self.__initialised = False
        self.ball_path: Path = None
        self.secrets_path: Path = None
        self.potfile_path: Path = None
        self.done_path: Path = None
        self.done_list: List[Dict[str, Any]] = []
        self.level: int = 1
        self.ball: inferno.BallType = None
        self.work_store = None

        self.config_path = config_path
        self.refresh_config()

    def serve(self):
        self._serve_thread.start()
        self._dead_client_thread.start()
        self._ball_cracked_thread.start()
        self._print_thread.start()

    def shutdown(self):
        self._lock.acquire()
        try:
            self._rpc.shutdown()
            self._dead_client_thread.cancel()
            self._ball_cracked_thread.cancel()
            self._print_thread.cancel()
        finally:
            self._lock.release()

        self._serve_thread.join()
        self._dead_client_thread.join()
        self._ball_cracked_thread.join()
        self._print_thread.join()

    def _print_output(self):
        self._lock.acquire()
        try:
            # TODO
            pass
        finally:
            self._lock.release()

    def _check_dead_clients(self):
        self._lock.acquire()
        try:
            now = pendulum.now()
            clients_to_remove = []
            for name, client in self._clients.items():
                if CLIENT_DEAD_THRESHOLD < now - client.last_heartbeat:
                    self._error_lines.append(_OutputLine(
                        'server',
                        'No heartbeat from {} for more than {}, '
                        'removing it from the list of '
                        'living'.format(name, str(CLIENT_DEAD_THRESHOLD))))
                    clients_to_remove.append(name)
            for name in clients_to_remove:
                if self._clients[name].job is not None:
                    self.work_store.put_back_work_batch(self._clients[name].job)
                del self._clients[name]
        finally:
            self._lock.release()

    def _check_ball_cracked(self):
        new_ball = self.__get_next_ball()
        if new_ball is not None:
            self._output_lines.append(_OutputLine('server',
                                                  'Ball cracked!'))
            if 'easteregg' in new_ball:
                self._output_lines.append(_OutputLine(
                    'server', 'Whoah,  an  easter  egg '
                              '{}'.format(new_ball['easteregg'])))
            elif len(new_ball) != 3:
                self._output_lines.append(_OutputLine(
                    'server', 'The new ball has {} keys: '
                              '{}'.format(len(new_ball), new_ball.keys())))
            self.refresh_config()

    def __work_batch_from_job_dict(self,
                                   job: Dict[str, str]) -> Optional[WorkBatch]:
        wordlist_re = regex.compile(r'^(?P<name>.+)'
                                    r'_(?P<begin>\d+)_(?P<end>\d+)$')
        hashlist_re = regex.compile(r'^(?P<name>.+)_(?P<hash_type>[^_]+)'
                                    r'_(?P<begin>\d+)_(?P<end>\d+)$')
        mw = wordlist_re.match(job['wordlist_name'])
        mh = hashlist_re.match(job['hashlist_name'])
        if not (mw and mh):
            return None
        wordlist = mw['name']
        wordlist_range = Range(mw['begin'], mw['end'])
        hashlist = mh['name']
        hashlist_range = Range(mh['begin'], mh['end'])

        return WorkBatch.for_work_store(self.work_store,
                                        wordlist,
                                        hashlist,
                                        job['hash_type'],
                                        wordlist_range,
                                        hashlist_range)

    def _receive_heartbeat(self,
                           name: str,
                           status: str,
                           job: Optional[Dict[str, str]] = None,
                           progress: Optional[Dict[str, Any]] = None) \
            -> Dict[str, Any]:
        self._lock.acquire()
        try:
            client = self._clients.setdefault(name, Client(name))

            prev_heartbeat = client.last_heartbeat
            prev_job = client.job

            client.last_heartbeat = pendulum.now()
            client.status = ClientStatus.from_name(status)
            client.progress = ClientProgress.from_dict(progress) \
                if progress is not None else None
            wb = client.job = self.__work_batch_from_job_dict(job) \
                if job is not None else None

            response = {}

            if job is not None:  # client is running something
                if wb is None or not self.work_store.should_be_in_progress(wb):
                    if client.status == ClientStatus.RUNNING:
                        # Whatever the client is doing shouldn't be done, so
                        # stop it
                        response['kill'] = True
                        self._error_lines.append(_OutputLine(
                            'server',
                            'Client {} is doing the wrong thing ({}), '
                            'killing'.format(name, job)))
                else:
                    if client.status == ClientStatus.FINISHED:
                        # Client done
                        done_dict = {
                            'wordlist': wb.wordlist.name,
                            'hashlist': wb.hashlist.name,
                            'hash_type': wb.hashlist.hash_type,
                            'wordlist_range': astuple(wb.wordlist_range),
                            'hashlist_range': astuple(wb.hashlist_range)
                        }
                        if done_dict not in self.done_list:
                            self.done_list.append(done_dict)
                            self.done_path.write_file(
                                json.dumps(self.done_list))
                            self.work_store.mark_done(wb)
                    if wb not in self.work_store.in_progress \
                            and client.status == ClientStatus.RUNNING:
                        # The client is doing this and it should be done, so
                        # mark it as in progress
                        self.work_store.add_to_in_progress(wb)
                    if prev_job != wb and prev_job is not None:
                        # The client is doing something other that what we
                        # remembered, so put the remembered stuff back in to_do
                        self.work_store.put_back_work_batch(client.job)
                        self._error_lines.append(_OutputLine(
                            'server',
                            'I thought client {} was doing {}, but seems to be '
                            'doing {} now. Putting old work batch back in '
                            'to_do'.format(name, prev_job, job)))

            response['outfile'] = [[x.hash, x.password]
                                   for x in self._passwords_discovered
                                   if x.when > prev_heartbeat]
            if len(response['outfile']) == 0:
                del response['outfile']

            return response
        except (KeyError, TypeError) as e:
            self._error_lines.append(_OutputLine('server',
                                                 'Invalid heartbeat from {}: '
                                                 '{}'.format(name, repr(e))))
            return {}
        finally:
            self._lock.release()

    def _receive_work_request(self,
                              name: str,
                              hash_type: Optional[str] = None) \
            -> Dict[str, Any]:
        self._lock.acquire()
        try:
            wb = self.work_store.request_work_batch(max_wordlist_size=2 ** 17,
                                                    hash_type=hash_type)

            if wb is not None:
                client = self._clients.setdefault(name, Client(name))

                # store it for the client because it is already marked as
                # in_progress
                if client.job is not None:
                    self.work_store.put_back_work_batch(client.job)
                client.job = wb

                wordlist_name = '{}_{}_{}'.format(wb.wordlist.name,
                                                  wb.wordlist_range.begin,
                                                  wb.wordlist_range.end)
                hashlist_name = '{}_{}_{}_{}'.format(wb.hashlist.name,
                                                     wb.hashlist.hash_type,
                                                     wb.hashlist_range.begin,
                                                     wb.hashlist_range.end)
                pot = inferno.get_pot(self.potfile_path)

                response = {
                    'wordlist': {
                        'name': wordlist_name,
                        'lines': wb.wordlist.lines[wb.wordlist_range.slice()]
                    },
                    'hashes': {
                        'name': hashlist_name,
                        'lines': wb.hashlist.lines[wb.hashlist_range.slice()]
                    },
                    'potfile': pot
                }
                return response
            else:
                return {}
        finally:
            self._lock.release()

    def _receive_new_password(self,
                              name: str,
                              hashed: str,
                              password: str):
        self._lock.acquire()
        try:
            self._passwords_discovered.append(_PasswordListItem(pendulum.now(),
                                                                hashed,
                                                                password))
            self.potfile_path.write_file('{}:{}\n'.format(hashed, password),
                                         'a')

        finally:
            self._lock.release()

    def _receive_output_line(self, name: str, line: str):
        self._lock.acquire()
        try:
            self._output_lines.append(_OutputLine(name, line))
        finally:
            self._lock.release()

    def _receive_error_line(self, name: str, line: str):
        self._lock.acquire()
        try:
            self._error_lines.append(_OutputLine(name, line))
        finally:
            self._lock.release()

    def _receive_bye(self, name: str):
        self._lock.acquire()
        try:
            if name in self._clients:
                if self._clients[name].job is not None:
                    self.work_store.put_back_work_batch(self._clients[name].job)
                del self._clients[name]
        finally:
            self._lock.release()

    def __get_next_ball(self,
                        current_ball: Optional[inferno.BallType] = None,
                        secret: Optional[str] = None) \
            -> Optional[inferno.BallType]:
        try:
            current_ball = current_ball or self.ball
            secret = secret or self.__get_next_secret()
            new_ball = inferno.descend(current_ball, secret)
            return new_ball
        except (UnicodeDecodeError, JSONDecodeError):
            return None

    def __get_next_secret(self,
                          current_ball: Optional[inferno.BallType] = None,
                          pot: Optional[inferno.PotType] = None) -> str:
        current_ball = current_ball or self.ball
        pot = pot or inferno.get_pot(self.potfile_path)
        return inferno.get_secret(current_ball, pot)

    def refresh_config(self):
        self._lock.acquire()
        try:
            config = json.loads(self.config_path.read_file())

            ball_path = Path(config['ball_path'])
            secrets_path = Path(config['secrets_path'])
            potfile_path = Path(config['potfile_path'])
            done_path = Path(config.get('done_path', 'done.json'))

            work_store = WorkStore()

            # ball
            ball = inferno.get_ball(ball_path, secrets_path=None)
            pot = inferno.get_pot(self.potfile_path)
            level = 1
            secrets = secrets_path.read_file().splitlines()
            for secret in secrets:
                next_ball = self.__get_next_ball(ball, secret)
                if next_ball is not None:
                    ball = next_ball
                    level += 1
                else:
                    break
            # rewrite secrets file, only with the secrets that worked
            if level < len(secrets) + 1:
                secrets_path.write_file('\n'.join(secrets[:level - 1]) + '\n')
            # see if there are more secrets already found
            while True:
                secret = self.__get_next_secret(ball, pot)
                next_ball = self.__get_next_ball(ball, secret)
                if next_ball is not None:
                    ball = next_ball
                    secrets_path.write_file(secret + '\n', 'a')
                else:
                    break

            # hashlist
            for hash_type, checker, priority in HASH_TYPE_CHECKER_PRIORITY:
                hl = HashList('level_{}'.format(level),
                              [x for x in ball['hashes'] if checker(x)],
                              hash_type,
                              priority)
                work_store.add_hashlist(hl)

            # wordlist
            wordlists = config['wordlists'].get(str(level), None) \
                        or config['wordlists']['default']
            for wordlist in wordlists:
                path = wordlist['path']
                path = Path(path)
                if not path.exists():
                    self._error_lines.append(_OutputLine(
                        'server',
                        'Wordlist {} does not exists, skipping'.format(path)))
                    continue
                wl = Wordlist(path, wordlist.get('priority', 1))
                work_store.add_wordlist(wl)

            # done_list
            done_list = json.loads(done_path.read_file())
            for done in done_list:
                try:
                    wb = WorkBatch.for_work_store(
                        work_store,
                        done['wordlist'],
                        done['hashlist'],
                        done['hash_type'],
                        Range(*done['wordlist_range']),
                        Range(*done['hashlist_range']))
                    work_store.mark_done(wb)
                except (KeyError, ValueError, TypeError):
                    pass

            # overwrite old values
            self.ball_path = ball_path
            self.secrets_path = secrets_path
            self.potfile_path = potfile_path
            self.done_path = done_path
            self.work_store = work_store
            self.level = level
            self.ball = ball
            self.done_list = done_list
            self.__initialised = True
        except (UnicodeDecodeError, JSONDecodeError, KeyError, ValueError,
                FileNotFoundError) as e:
            if self.__initialised:
                self._error_lines.append(_OutputLine('server',
                                                     'Invalid config! Keeping '
                                                     'old values'))
                self._error_lines.append(_OutputLine('server', repr(e)))
                return
            else:
                raise Exception('Invalid config!') from e
        finally:
            self._lock.release()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('address',
                        help='Hostname or IP where the server should listen')
    parser.add_argument('port',
                        help='Port on which the server should listen')
    parser.add_argument('config',
                        help='Configuration file path',
                        default='config.json',
                        type=Path)
    args = parser.parse_args()

    server = Server(args.address, args.port, Path(args.config))
    try:
        server.serve()
    except KeyboardInterrupt:
        print('Shutting server down...')
        server.shutdown()
