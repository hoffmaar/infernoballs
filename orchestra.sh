#!/bin/bash

# set -o xtrace

if [ -z "${JOHN}" ]; then
    JOHN=/mnt/Carbon/projects/cs4400/JohnTheRipper/run/john
fi

if [ -z "${HASHCAT}" ]; then
    HASHCAT=/usr/bin/hashcat
fi

if [ -z "${POT}" ]; then
    POT=/home/aron/.hashcat/hashcat.potfile
fi

if [ -z "${WORDS}" ]; then
    WORDS=/usr/share/dict/words
fi

if [ -z "${ROCKYOU}" ]; then
    ROCKYOU=/usr/share/dict/rockyou.txt
fi

if [ -z "${HASHES}" ]; then
    HASHES=hashes_1
fi

if [ -z "${FIVE_CHAR_LIST}" ]; then
    FIVE_CHAR_LIST=/mnt/Carbon/projects/cs4400/practical5/5char_list
fi

function hashcat_f {
    ${HASHCAT} --potfile-path="${POT}" "${@}" | grep -v 'Token encoding exception\|Signature unmatched\|Separator unmatched'
}

function john_f {
    if [ -z ${OMP_NUM_THREADS} ]; then
        OMP_NUM_THREADS=10
    fi
    OMP_NUM_THREADS="${OMP_NUM_THREADS}" ${JOHN} "${@}" --pot="${POT}"
}

base64_a_to_b='{/[^:]:./b strip_sol; y/+/./; b a_to_b_fin; :strip_sol; h; s/^[^:]+:(.+)$/\1/; x; s/^([^:]+):.+$/\1/; y/+/./; G; s/\n/:/; :a_to_b_fin;}'
base64_b_to_a='{/[^:]:./b strip_sol; y/./+/; b b_to_a_fin; :strip_sol; h; s/^[^:]+:(.+)$/\1/; x; s/^([^:]+):.+$/\1/; y/./+/; G; s/\n/:/; :b_to_a_fin;}'

function md5_to_hashcat_md5 {
    sed -re '/^\$1\$.+$/'"${base64_a_to_b}"''
}
function hashcat_md5_to_md5 {
    sed -re '/^\$1\$.+$/'"${base64_b_to_a}"''
}

function des_to_hashcat_des {
    sed -re '/^.{13}$/'"${base64_a_to_b}"''
}
function hashcat_des_to_des {
    sed -re '/^.{13}$/'"${base64_b_to_a}"''
}

function sha512_to_hashcat_sha512 {
    sed -re '/^\$6\$.+$/'"${base64_a_to_b}"''
}
function hashcat_sha512_to_sha512 {
    sed -re '/^\$6\$.+$/'"${base64_b_to_a}"''
}

function sha256_to_hashcat_sha256 {
    sed -re '/^\$5\$.+$/'"${base64_a_to_b}"''
}
function hashcat_sha256_to_sha256 {
    sed -re '/^\$5\$.+$/'"${base64_b_to_a}"''
}

function pbkdf2_to_sha {
    sed -re '/^\$pbkdf2[-_]sha256/'"${base64_b_to_a}"'; s/^\$pbkdf2[-_]sha256\$([[:digit:]]+)\$([[:alnum:]/.+=]+)\$([[:alnum:]/.+=]+)/sha256:\1:\2:\3/;'
}
function sha_to_pbkdf2 {
    sed -re 's/^sha256:([[:digit:]]+):([[:alnum:]/=+.]+):([[:alnum:]/=+.]+)/$pbkdf2-sha256$\1$\2$\3/; T; '"${base64_a_to_b}"
}

function filter_to_line_length { # argument 1: length
    awk "{ if (length(\$0) == ${1}) print }"
}

function truncate_descrypt {
    sed -re 's/^([a-zA-Z0-9./]{13}):(.{7}).$/\1:\2/'
}

function colon_to_space {
    sed -re 's/^([^:]+):(.+)$/\1 \2/'
}

function dehexify {
	sed -re 'h;s/(.+)\$HEX\[[a-fA-F0-9]+\]$/\1/;T nohex;x;s/.+HEX\[([a-fA-F0-9]+)\]$/echo \1 | xxd -r -p/e;x;G;s/\n//;b;:nohex;z;x;'
}

function export_passwords {
    for h in ${hashcat_hashes}; do hashcat_f --show -m ${h} ${HASHES}; done | hashcat_md5_to_md5 | hashcat_des_to_des | hashcat_sha512_to_sha512 | hashcat_sha256_to_sha256 | sha_to_pbkdf2 | truncate_descrypt | colon_to_space | dehexify
}

function print_pot {
    cat "${POT}" | sha_to_pbkdf2 | grep ':' | sed -re 's/^[^:]+:(.+)$/\1/' | awk '{ print length, $0 }' | sort -sn | sed -re 's/^[^ ]+ (.+)$/\1/' | dehexify | uniq
}

function run_all_hashcat { # argument 1: hash type
	format=${1}
	shift
    # trap | read save_trap
    (
        tmpfile=$(mktemp);
        # trap "rm -f -- \"${tmpfile}\"; kill -QUIT ${$}" INT TERM HUP EXIT
		cat "${HASHES}" | md5_to_hashcat_md5 | des_to_hashcat_des | sha512_to_hashcat_sha512 | sha256_to_hashcat_sha256 | pbkdf2_to_sha > "${tmpfile}";

        # print_pot | hashcat_f -a 0 -m "${format}" -O -w 3 --session="cs4400_practical5_${format}_loopback" ${@} ${tmpfile};
        hashcat_f -a 3 -m "${format}" -O -w 3 ${tmpfile} --session="cs4400_practical5_${format}_5chars" ${@} '?l?l?l?l?l';
        hashcat_f -a 1 -m "${format}" -O -w 3 --session="cs4400_practical5_${format}_dict44" ${@} "${tmpfile}" english_4.txt english_4.txt;
        hashcat_f -a 0 -m "${format}" -O -w 3 --session="cs4400_practical5_${format}_rockyou" ${@} "${tmpfile}" "${ROCKYOU}";

        rm -f -- "${tmpfile}";
    )
    # trap - 1 2 3 15 # reset trap
    # eval "${save_trap}"
}

function run_all_john { # argument 1: hash format
    print_pot | john_f --format="${1}" "${HASHES}" --stdin --session="cs4400_practical5_${1}_loopback";
    touch lock; while true; do pwgen -0 -A 5 -1; if [ ! -f lock  ]; then break; fi; done | uniq | (john_f --format="${1}" "${HASHES}" --session="cs4400_practical5_${1}_5chars" --stdin; rm -f lock)
    hashcat -a 1 -O -w 3 --stdout english_4.txt english_4.txt | john_f --format="${1}" "${HASHES}" --stdin --session="cs4400_practical5_${1}_dict44";
    john_f --format="${1}" "${HASHES}" --wordlist="${ROCKYOU}" --session="cs4400_practical5_${1}_rockyou";
}

declare hashcat_hashes=(10900 15100 1800)
declare john_hashes=()

function main {
    # create 4-character dictionary
    cat "${WORDS}" | filter_to_line_length 4 > english_4.txt;

    # reformat PBKDF2 hashes for hashcat
    cat "${HASHES}" | pbkdf2_to_sha | sponge "${HASHES}";

    for h in ${hashcat_hashes[@]}; do
        run_all_hashcat "${h}"
    done

    for h in ${john_hashes[@]}; do
        run_all_john "${h}"
    done
}

if [ "${1}" != "--source-only" ]; then
    main "${@}"
fi
