import itertools

from dataclasses import dataclass, field
from typing import *
from unipath import Path


@dataclass
class Wordlist:
    path: Path
    name: str
    lines: List[str]
    priority: int

    def __init__(self, path: Path, priority: int = 1):
        self.path = path
        self.name = path.stem()
        self.lines = path.read_file().splitlines()
        self.priority = priority

    def __eq__(self, other: 'Wordlist') -> bool:
        return self.name == other.name


HashType = str


@dataclass
class HashList:
    name: str
    lines: List[str]
    hash_type: HashType
    priority: int = 1

    def __eq__(self, other: 'HashList') -> bool:
        return self.name == other.name and self.hash_type == other.hash_type


@dataclass
class Range:
    begin: int
    end: int

    @staticmethod
    def __process_args(other: Union[int, 'Range'],
                       other_end: Optional[int] = None) -> Tuple[int, int]:
        """
        Methods of the Range class accepts either a range, or two
        integers. Since method overloading is not a thing in python,
        this method helps them internally pre-process the arguments
        """
        if other_end is not None:
            return other, other_end
        else:
            return other.begin, other.end

    def intersects(self,
                   other: Union[int, 'Range'],
                   other_end: Optional[int] = None) -> bool:
        b, e = self.__process_args(other, other_end)
        return b < self.end and self.begin < e

    def intersection(self,
                     other: Union[int, 'Range'],
                     other_end: Optional[int] = None) -> 'Range':
        b, e = self.__process_args(other, other_end)
        if self.intersects(b, e):
            return Range(max(self.begin, b),
                         min(self.end, e))
        return Range(self.begin, self.begin)

    def removed(self,
                other: Union[int, 'Range'],
                other_end: Optional[int] = None) -> List['Range']:
        b, e = self.__process_args(other, other_end)
        result = []
        if self.begin < b:  # first piece
            result.append(Range(self.begin, min(b, self.end)))
        if e < self.end:  # last piece
            result.append(Range(max(self.begin, e), self.end))
        return result

    def joinable(self,
                 other: Union[int, 'Range'],
                 other_end: Optional[int] = None) -> bool:
        b, e = self.__process_args(other, other_end)
        return b <= self.end or self.begin <= e

    def joined(self,
               other: Union[int, 'Range'],
               other_end: Optional[int] = None) -> Optional['Range']:
        b, e = self.__process_args(other, other_end)

        if self.joinable(other, other_end):
            return Range(min(self.begin, b), max(self.end, e))
        return None

    def __len__(self) -> int:
        return self.end - self.begin

    def slice(self) -> slice:
        return slice(self.begin, self.end, 1)


@dataclass
class WorkBatch:
    wordlist: Wordlist
    hashlist: HashList
    wordlist_range: Range
    hashlist_range: Range

    @classmethod
    def for_work_store(cls,
                       work_store: 'WorkStore',
                       wordlist: Union[str, Wordlist],
                       hashlist: Union[str, HashList],
                       hash_type: str,
                       wordlist_range: Range,
                       hashlist_range: Range) -> 'WorkBatch':
        if isinstance(wordlist, str):
            wordlist = work_store.get_wordlist(wordlist)
        elif wordlist not in work_store.wordlists:
            wordlist = None

        if isinstance(hashlist, str):
            hashlist = work_store.get_hashlist(hashlist, hash_type)
        elif hashlist not in work_store.hashlists:
            hashlist = None

        if wordlist is None:
            raise ValueError('Wordlist {} not found amongst known '
                             'wordlists'.format(wordlist))
        if hashlist is None:
            raise ValueError('Hashlist {} not found amongst known '
                             'hashlists'.format(hashlist))

        if wordlist_range.begin < 0 or wordlist_range.end > len(wordlist.lines):
            raise ValueError('Invalid wordlist range {}'.format(wordlist_range))

        if hashlist_range.begin < 0 or hashlist_range.end > len(
                hashlist.lines):
            raise ValueError('Invalid hashlist range {}'.format(hashlist_range))

        return cls(wordlist, hashlist, wordlist_range, hashlist_range)


@dataclass
class WorkStore:
    wordlists: List[Wordlist] = field(default_factory=set)
    hashlists: List[HashList] = field(default_factory=set)
    to_do: List[WorkBatch] = field(default_factory=set)
    in_progress: List[WorkBatch] = field(default_factory=set)

    def add_wordlist(self, wordlist: Wordlist):
        if len(wordlist.lines) == 0:
            return
        self.wordlists.append(wordlist)
        for hashlist in self.hashlists:
            self.to_do.append(WorkBatch(wordlist, hashlist,
                                        Range(0, len(wordlist.lines)),
                                        Range(0, len(hashlist.lines))))

    def remove_wordlist(self, wordlist: Wordlist):
        self.wordlists = [wl for wl in self.wordlists
                          if wl != wordlist]
        self.to_do = [x for x in self.to_do
                      if x.wordlist != wordlist]
        self.in_progress = [x for x in self.in_progress
                            if x.wordlist != wordlist]

    def add_hashlist(self, hashlist: HashList):
        if len(hashlist.lines) == 0:
            return
        self.hashlists.append(hashlist)
        for wordlist in self.wordlists:
            self.to_do.append(WorkBatch(wordlist, hashlist,
                                        Range(0, len(wordlist.lines)),
                                        Range(0, len(hashlist.lines))))

    def remove_hashlist(self, hashlist: HashList):
        self.hashlists = [hl for hl in self.hashlists
                          if hl != hashlist]
        self.to_do = [x for x in self.to_do
                      if x.hashlist != hashlist]
        self.in_progress = [x for x in self.in_progress
                            if x.hashlist != hashlist]

    def request_work_batch(self,
                           max_wordlist_size: int = 0,
                           max_hashlist_size: int = 0,
                           hash_type: Union[str, HashType, None] = None) \
            -> Optional[WorkBatch]:
        result = next(
            (wb for wb in sorted(self.to_do,
                                 key=lambda x: (x.hashlist.priority,
                                                x.wordlist.priority),
                                 reverse=True)
             if hash_type is None or wb.hashlist.hash_type == hash_type),
            None
        )
        if result is not None:
            # Make it a new object, so we don't modify the old one
            result = WorkBatch(result.wordlist, result.hashlist,
                               result.wordlist_range, result.hashlist_range)

            # split off wordlist if necessary
            if 0 < max_wordlist_size < len(result.wordlist_range):
                result.wordlist_range = Range(0, max_wordlist_size)

            # split off hashlist
            if 0 < max_hashlist_size < len(result.hashlist_range):
                result.hashlist_range = Range(0, max_hashlist_size)

            # remove the WorkBatch piece used from self.to_do,
            # put the piece used in self.in_progress
            self.add_to_in_progress(result)

        return result

    def put_back_work_batch(self, target: WorkBatch):
        to_be_removed = []
        # make it a different object
        result = WorkBatch(target.wordlist, target.hashlist,
                           target.wordlist_range, target.hashlist_range)
        # merge hashlists
        for wb in self.to_do:
            if all([wb not in to_be_removed,
                    wb.wordlist == target.wordlist_range,
                    wb.hashlist == target.hashlist,
                    wb.wordlist_range == target.wordlist_range,
                    wb.hashlist_range.joinable(target.hashlist_range)]):
                hlr = result.hashlist_range.joined(wb.hashlist_range)
                if hlr is not None:
                    to_be_removed.append(wb)
                    result.hashlist_range = hlr
        # merge wordlists
        for wb in self.to_do:
            if all([wb.wordlist == target.wordlist,
                    wb.hashlist == target.hashlist,
                    wb.wordlist_range.joinable(target.wordlist_range),
                    wb.hashlist_range == target.hashlist_range]):
                wlr = result.wordlist_range.joined(wb.wordlist_range)
                if wlr is not None:
                    to_be_removed.append(wb)
                    result.wordlist_range = wlr

        # remove from self.in_progress
        if target in self.in_progress:
            self.in_progress.remove(target)
        # remove merged stuff from to_do
        for x in to_be_removed:
            if x in self.to_do:
                self.to_do.remove(x)
        # add result to to_do
        self.to_do.append(result)

    def remove_from_to_do(self, target: WorkBatch):
        put_back = []
        to_be_removed = []

        for td in self.to_do:
            if all([td.wordlist == target.wordlist,
                    td.hashlist == target.hashlist,
                    td.wordlist_range.intersects(target.wordlist_range),
                    td.hashlist_range.intersects(target.hashlist_range)]):
                to_be_removed.append(td)
                put_back += [
                    WorkBatch(td.wordlist, td.hashlist, r, td.hashlist_range)
                    for r in td.wordlist_range.removed(target.wordlist_range)
                ]
                put_back += [
                    WorkBatch(td.wordlist, td.hashlist, td.wordlist_range, r)
                    for r in td.hashlist_range.removed(target.hashlist_range)
                ]

        self.to_do = [x for x in self.to_do
                      if x not in to_be_removed]
        self.to_do += put_back

    def should_be_in_progress(self, target: WorkBatch):
        if target in self.in_progress:
            return True

        # if target is a to_do
        work_left = [(target.wordlist_range, target.hashlist_range)]
        for shard in work_left:
            consumed = False
            for td in self.to_do:
                if all([td.wordlist == target.wordlist,
                        td.hashlist == target.hashlist,
                        td.wordlist_range.intersects(shard[0]),
                        td.hashlist_range.intersects(shard[1])]):
                    consumed = True

                    wli = td.wordlist_range.intersection(shard[0])
                    hli = td.hashlist_range.intersection(shard[1])

                    wlr_list = shard[0].removed(td.wordlist_range)
                    hlr_list = shard[1].removed(td.hashlist_range)
                    wlr_list = [x for x in wlr_list if len(x) != 0]
                    hlr_list = [x for x in hlr_list if len(x) != 0]

                    work_left += [(wlr, hli) for wlr in wlr_list]
                    work_left += [(wli, hlr) for hlr in hlr_list]
                    work_left += list(itertools.product(wlr_list, hlr_list))
                    break

            if not consumed:
                return False

            if len(work_left) == 0:
                break

        return True

    def add_to_in_progress(self, target: WorkBatch):
        self.remove_from_to_do(target)
        self.in_progress.append(target)

    def mark_done(self, target: WorkBatch):
        self.remove_from_to_do(target)
        self.in_progress = [x for x in self.in_progress
                            if x != target]

    def get_wordlist(self, wordlist: str) -> Optional[Wordlist]:
        for wl in self.wordlists:
            if wl.name == wordlist:
                return wl
        return None

    def get_hashlist(self,
                     hashlist: str,
                     hash_type: HashType) -> Optional[HashList]:
        for hl in self.hashlists:
            if hl.name == hashlist and hl.hash_type == hash_type:
                return hl
        return None
