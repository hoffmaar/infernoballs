# Protocollo dell'Inferno
## Client-Server RPCs
### Heartbeat: `heartbeat`
Sent every second or so by the client to the server

Request base sent client on every heartbeat:
```json
{
    "name": "(str) The name of the client, given as a command line argument",
    "status": "(str) (idle|running|error|finished)"
}
```

If the client status is `running` or `finished`, the heartbeat request is 
extended by:
```json
{
    "job": {
        "wordlist_name": "(str) The name given to the wordlist computed by the current job by the server",
        "hashes_name": "(str) The name given to the hashlist computed by the current job by the server",
        "hash_type": "(str) The type of hash being computed by client"
    }
}
```

If the client status is `running`, the heartbeat request is further extended by:
```json
{
    "progress": {
        "speed": ["(int) Array of the speeds (hashes/second) of each sub-worker (GPU)"],
        "utilisation": ["(float) List of fractions (out of 1) to which computation resources are utilised"],
        "guesses": ["(int) Number of guesses tried",
                    "(int) Total number of guesses to try"],
        "recovered": ["(int) Number of passwords recovered",
                      "(int) Total number of passwords"],
        "time_started": "(int) UTC timestamp when the current job began running",
        "eta": "(int) Estimated UTC timestamp when the current job will be done"
    }
}
```

Response sent by the server:
```json
{
    "kill": "(bool) (optional) The client is computing the wrong thing, stop",
    "outfile": ["[(str) hash, (str) pass] (optional) An array of relevant new passwords discovered by other clients"]
}
```

### New password found: `new_password`
Sent when a new password is found by the client
```json
{
    "name": "(str) Name of the client, specified as a command line argument",
    "hashed": "(str) The encrypted form of the password found",
    "password": "(str) The password found"
}
```

### Request work: `request_work`
Sent when by the client when its status is `idle` or `finished`
```json
{
    "name": "(str) Name of the client, specified as a command line argument",
    "hash_type": "(str) (optional) Specialised hash type of the client, specified as a command line argument"
}
```

The response sent by the server if there is work to be done:
```json
{
    "hashes": {
        "name": "(str) The name given to the hashlist, stored as 'hashes_name'",
        "lines": ["(str) List of hashes to be cracked"]
    },
    "wordlist": {
        "name": "(str) The name given to the wordlist, stored as 'wordlist_name'",
        "lines": ["(str) List of guesses to try"]
    },
    "potfile": ["[(str) hash, (str) pass] List of passwords already found"]
}
```
Otherwise, the response is empty (`{}`).

### Output line: `output_line`
Sent when there is an unexpected output on the `stdout` of the client
```json
{
    "name": "(str) Name of the client, specified as a command line argument",
    "line": "(str) Text of the error"
}
```

### Error line: `error_line`
Sent when there is output on `stderr` or an error hash happened on the client 
side that requires immediate human attention:
```json
{
    "name": "(str) Name of the client, specified as a command line argument",
    "line": "(str) Text of the error"
}
```

### Bye: `bye`
Sent when the client has been manually quit, notifying the server of this
```json
{
    "name": "(str) The name of the client, given as a command line argument"
}
```

## Server config
Path to this file is given as a command line argument
```json
{
    "wordlists": {
        "(str) level": [
            {
                "path": "(str) Path of the wordlist",
                "priority": "(int) (optional) Priority assigned to the wordlist on this level"
            }
        ]
    },
    "ball_path": "(str) Path of the inferno-ball",
    "secrets_path": "(str) Path where uncovered secrets go",
    "potfile_path": "(str) Path of global potfile",
    "done_path": "(str) Path to for a file to store, which work batches have been completed"
}
```