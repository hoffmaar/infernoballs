#!/usr/bin/python

import argparse

from client_status import ClientStatus
from hashcat import *
import jsonrpclib
import time


class Client(object):
    def __init__(self,
                 server_url: str,
                 name: str,
                 preferred_hash: str = None,
                 hashes_path: Path = Path('hashes'),
                 wordlist_path: Path = Path('wordlist'),
                 potfile_path: Path = Path('potfile')):
        self.name = name
        self.preferred_hash = preferred_hash
        self.hashes_path = hashes_path
        self.wordlist_path = wordlist_path
        self.potfile_path = potfile_path
        self._rpc = jsonrpclib.Server(server_url)
        self._worker = None
        self._last_pot = self._read_pot()

    def heartbeat(self):
        status = dict()
        status['name'] = self.name

        # worker stuff
        status.update(self._translate_worker_status())
        if self._worker is not None:
            for error_line in self._worker.read_stderr():
                self._send_error_line(error_line)

        # send heartbeat
        response = self._rpc.heartbeat(status)

        # kill if asked
        if response.get('kill', False):
            self._kill_worker()
            status['status'] = ClientStatus.IDLE

        # update outfile
        self._add_to_outfile(response.get('outfile', []))

        # request more work if needed
        if status['status'] in [ClientStatus.IDLE, ClientStatus.FINISHED]:
            self._request_work()

    def _translate_worker_status(self) -> Dict:
        status = {}

        if self._worker is None:
            status['status'] = ClientStatus.IDLE
        else:
            ws = self._worker.status_update()
            if ws.status_update == HashcatStatus.RUNNING:
                status['status'] = ClientStatus.RUNNING
                status['progress'] = ws.progress
                status['speed'] = ws.speed,
                status['recovered'] = ws.recovered,
                status['time_started'] = ws.time_started.timestamp()
                status['eta'] = ws.eta.timestamp()
                status['utilisation'] = ws.utilisation

                # parrot these ones back
                status['hashes_name'] = self.hashes_name
                status['wordlist_name'] = self.wordlist_name
                status['hash_type'] = self.hash_type
            elif ws.status_update == HashcatStatus.FINISHED:
                status['status'] = ClientStatus.FINISHED
            else:
                status['status'] = ClientStatus.ERROR

            status['new_cracks'] = ws.new_cracks

        # can't send over enums
        status['status'] = repr(status['status'])

        return status

    def _kill_worker(self):
        if self._worker is None:
            return
        self._worker.kill()
        self._worker = None

    def _add_to_outfile(self, outs: List[str]):
        if len(outs) == 0:
            return
        if self._worker is None:
            return
        self._worker.add_to_outfile(outs)

    def _request_work(self):
        response = self._rpc.request_work(self.name)

        for k in ['hashes', 'wordlist', 'potfile']:
            if k not in response:
                self._send_error_line('Bad work sent: {} not in '
                                      'response'.format(k))
                return

        self.hashes_path.write_file('\n'.join(response['hashes']))
        self.wordlist_path.write_file('\n'.join(response['wordlist']))
        self.potfile_path.write_file('\n'.join(response['potfile']))

        self.hashes_name = response['hashes_name']
        self.wordlist_name = response['wordlist_name']
        self.hash_type = response['hash_type']

        if self._worker is not None:
            self._worker.kill()

        # TODO no cracker
        if response['cracker'] == 'hashcat':
            self._worker = Hashcat(hash_type=int(response['hash_type']),
                                   hashes=self.hashes_path,
                                   wordlist=self.wordlist_path)
        else:
            self._send_error_line('Cracker {} unknown, '
                                  'work rejected'.format(response['cracker']))

        self._last_pot = self._read_pot()

    def _send_error_line(self, line: str):
        self._rpc.error_line(self.name, line)

    def send_bye(self):
        self._rpc.bye(self.name)

    def _read_pot(self) -> List[str]:
        if self._worker is None:
            return list()
        if not self._worker.potfile.exists():
            return list()
        return self._worker.potfile.read_file().splitlines()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('server', help='URL of the server')
    parser.add_argument('name', help='Name of the client')
    parser.add_argument('hash_type',
                        help='Request to only compute particular hash',
                        default=None,
                        required=False)
    args = parser.parse_args()

    client = Client(args.url, args.name, args.hash_type)
    try:
        while True:
            client.heartbeat()
            time.sleep(1)
    except KeyboardInterrupt:
        print('Saying bye to server, then stopping...')
        client.send_bye()
