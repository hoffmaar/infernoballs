from enum import Enum


class ClientStatus(Enum):
    IDLE = 0
    RUNNING = 1
    ERROR = 2
    FINISHED = 3

    def __repr__(self):
        return self.name.lower()

    @classmethod
    def from_name(cls, name: str):
        for s in cls:
            if repr(s) == name:
                return s
        return cls.ERROR
