#!/usr/bin/python

from inferno import *

for i in range(len(SECRETS_PATH.read_file().splitlines()) + 1):
    ball = get_ball(level=i)
    if 'easteregg' in ball:
        print('Level {:d}'.format(i + 1))
        print(ball['easteregg'])
        print('')
    elif len(ball) > 3:
        print('Level {:d} ball keys'.format(i + 1))
        print('\n'.join(ball.keys()))
        print('')
